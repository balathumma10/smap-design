from distutils.core import setup


setup(
    name='SMAP Design',
    use_scm_version={'write_to': 'smap/version.py'},
    setup_requires=['setuptools_scm', 'setuptools_scm_git_archive'],
    description=(('Design primers for amplicon sequencing, in combination with '
                  'sgRNA design from third party software such as CRISPOR')),
    install_requires=['colorlog~=4.6.2'],
    extras_require={'docs': ['sphinx','sphinx_rtd_theme','sphinx-tabs<2.0.0']},
    entry_points={
        'console_scripts': [
            'smap = smap_design.__main__:main',
        ],
        'modules': [
            'design = smap_design.design:main',
        ]
    },
    packages=['smap_design'],
    python_requires='>=3.7.1',
    url="https://gitlab.com/truttink/smap"
)
